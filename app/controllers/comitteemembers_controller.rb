class ComitteemembersController < ApplicationController


  before_action :set_comitteemember, only: [:show, :edit, :update, :destroy]

  def index
    @comitteemembers = Comitteemember.all

  end

  def show
  end

  def new
    @comitteemember = Comitteemember.new
  end
  def edit
  end
 
  def create
    @comitteemember = Comitteemember.new(comitteemember_params)
    

    respond_to do |format|
      if @comitteemember.save
        format.html { redirect_to @comitteemember, notice: 'Comitteemember was successfully created.' }

      else
        format.html { render :new }

      end
    end
  end
    def update
    respond_to do |format|
      if @comitteemember.update(comitteemember_params)
        format.html { redirect_to @comitteemember, notice: 'Comitteemember was successfully updated.' }

      else
        format.html { render :edit }

      end
    end
  end

  def destroy
    @comitteemember.destroy
    respond_to do |format|
      format.html { redirect_to comitteemembers_url, notice: 'Comitteemember was successfully destroyed.' }

    end
  end

  private
    def set_comitteemember
      @comitteemember = Comitteemember.find(params[:id])
    end
        def comitteemember_params
      params.require(:comitteemember).permit(:fullname, :gender, :role, :email, :phaseno, :responsiblefor, :startdate, :duration)
    end
end
