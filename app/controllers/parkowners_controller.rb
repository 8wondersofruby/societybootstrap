class ParkownersController < ApplicationController
def index

     @parkowners = Parkowner.all
  end

  def edit
    @parkowner=Parkowner.find(params[:id])
  end

  def show
  	@parkowner=Parkowner.find(params[:id])
  end

  def new
     @parkowner = Parkowner.new
  end

  def update
    @parkowner=Parkowner.find(params[:id])
    if @parkowner.update(parkowner_params)
 
      redirect_to @parkowner
    else
      render 'edit'
  end
end

  def create
  	@parkowner=Parkowner.new(parkowner_params)
     
  	if @parkowner.save
  	redirect_to @parkowner
  else
    render 'new'
  end
end

  
private
def parkowner_params
      params.require(:parkowner).permit(:parkingno, :nooftwowheeler, :nooffourwheeler)
    end
end
