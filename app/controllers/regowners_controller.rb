class RegownersController < ApplicationController

 def index
  @search = Regowner.search do
    fulltext params[:search]
    
    end
@regowners=@search.results
  end
  def edit
    @regowner=Regowner.find(params[:id])
  end

  def show
  	@regowner=Regowner.find(params[:id])
  end

  def new
     @regowner =Regowner.new
  end

  def update
    @regowner=Regowner.find(params[:id])
    if @regowner.update(regowner_params)
 
      redirect_to @regowner
    else
      render 'edit'
  end
end

  def create
  	@regowner= Regowner.new(regowner_params)
     
  	if @regowner.save
  	redirect_to @regowner
  else
    render 'new'
  end
end

  
private
def regowner_params
      params.require(:regowner).permit(:firstname, :lastname, :email, :landlineno, :mobileno, :dob, :gender, :age, :nooffamily, :profession, :pancardno, :address,:image)
    end
end
