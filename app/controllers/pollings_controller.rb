class PollingsController < ApplicationController
  before_action :set_polling, only: [:show, :edit, :update, :destroy, :upvote, :downvote]

  # GET /pollings
  # GET /pollings.json

  def index
    @pollings = Polling.all
  end

  # GET /pollings/1
  # GET /pollings/1.json
  def show

  end

  # GET /pollings/new
  def new
    @polling = Polling.new
  end

  # GET /pollings/1/edit
  def edit
  end

  # POST /pollings
  # POST /pollings.json
  def create
    @polling = Polling.new(polling_params)

    respond_to do |format|
      if @polling.save
        format.html { redirect_to @polling, notice: 'Polling was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /pollings/1
  # PATCH/PUT /pollings/1.json
  def update
    respond_to do |format|
      if @polling.update(polling_params)
        format.html { redirect_to @polling, notice: 'Polling was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /pollings/1
  # DELETE /pollings/1.json
  def destroy
    @polling.destroy
    respond_to do |format|
      format.html { redirect_to pollings_url, notice: 'Polling was successfully destroyed.' }
    end
  end

  def upvote
    @polling.upvote_from current_user
    redirect_to pollings_path
  end

  def downvote
    @polling.downvote_from current_user
    redirect_to pollings_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_polling
      @polling = Polling.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def polling_params
      params.require(:polling).permit(:name, :age, :positiontype, :email_id, :wing, :flatno, :image)
    end
end