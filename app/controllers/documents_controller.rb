class DocumentsController < ApplicationController


  def index
  
     @documents = Document.all
  end

  def edit
       @document=Document.find(params[:id])
  end

  def show
  	@document=Document.find(params[:id])
  end

  def new
    @document=Document.new
  end

 def update
    @document=Document.find(params[:id])
    if @document.update(document_params)
 
      redirect_to @document
    else
      render 'edit'
  end
end

    
  def create
  	@document= Document.new(document_params)
  	if @document.save
  	 redirect_to documents_path, notice: "Document #{@document.title} has been uploaded ."
    else
      render 'new'
  end
end
  
private
def document_params
      params.require(:document).permit(:title,:attachment)
    end
end


