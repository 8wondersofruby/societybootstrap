class AccPaymentsController < ApplicationController
  before_action :set_acc_payment, only: [:show, :edit, :update, :destroy]

  # GET /acc_payments
  # GET /acc_payments.json
  def index
    @acc_payments = AccPayment.all
  end

  # GET /acc_payments/1
  # GET /acc_payments/1.json
  def show
  end

  # GET /acc_payments/new
  def new
  	  @acc_payment_type = ["Transfer","Cheque","Draft","Cash"]
    @acc_payment = AccPayment.new
  end

  # GET /acc_payments/1/edit
  def edit
  end

  # POST /acc_payments
  # POST /acc_payments.json
  def create
    @acc_payment = AccPayment.new(acc_payment_params)

    respond_to do |format|
      if @acc_payment.save
        format.html { redirect_to @acc_payment, notice: 'Acc payment was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /acc_payments/1
  # PATCH/PUT /acc_payments/1.json
  def update
    respond_to do |format|
      if @acc_payment.update(acc_payment_params)
        format.html { redirect_to @acc_payment, notice: 'Acc payment was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /acc_payments/1
  # DELETE /acc_payments/1.json
  def destroy
    @acc_payment.destroy
    respond_to do |format|
      format.html { redirect_to acc_payments_url, notice: 'Acc payment was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_acc_payment
      @acc_payment = AccPayment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def acc_payment_params
      params.require(:acc_payment).permit(:chq_desc, :chq_no, :bnk_name, :bnk_branch, :chq_date, :cash_desc, :paym_date, :drft_desc, :drft_no, :dbnk_name, :dbnk_branch, :drft_date, :ot_desc, :otbnk_name, :trn_no, :ot_date, :exbill_desc, :exbill_amount, :exbill_date)
    end
end
