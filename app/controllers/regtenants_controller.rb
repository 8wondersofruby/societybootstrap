class RegtenantsController < ApplicationController

  def index
  
     @regtenants = Regtenant.all
  end

  def edit
       @regtenant=Regtenant.find(params[:id])
  end

  def show
  	@regtenant=Regtenant.find(params[:id])
  end

  def new
    @regtenant=Regtenant.new
  end

 def update
    @regtenant=Regtenant.find(params[:id])
    if @regtenant.update(regtenant_params)
 
      redirect_to @regtenant
    else
      render 'edit'
  end
end

  def create
  	@regtenant= Regtenant.new(regtenant_params)
  	if @regtenant.save
  	 redirect_to @regtenant
    else
      render 'new'
  end
end
  
private
def regtenant_params
      params.require(:regtenant).permit(:ownerid,:firstname, :lastname, :email, :landlineno, :mobileno, :dob, :gender, :age, :nooffamily, :profession, :pancardno, :address,:image)
    end
end
