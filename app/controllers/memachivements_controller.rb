class MemachivementsController < ApplicationController


  before_action :set_memachivement, only: [:show, :edit, :update, :destroy]

  # GET /memachivements
  # GET /memachivements.json
  def index
    @memachivements = Memachivement.all
  end

  # GET /memachivements/1
  # GET /memachivements/1.json
  def show
  end

  # GET /memachivements/new
  def new
    @memachivement = Memachivement.new
  end

  # GET /memachivements/1/edit
  def edit
  end

  # POST /memachivements
  # POST /memachivements.json
  def create
    @memachivement = Memachivement.new(memachivement_params)

    respond_to do |format|
      if @memachivement.save
        format.html { redirect_to @memachivement, notice: 'Memachivement was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /memachivements/1
  # PATCH/PUT /memachivements/1.json
  def update
    respond_to do |format|
      if @memachivement.update(memachivement_params)
        format.html { redirect_to @memachivement, notice: 'Memachivement was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /memachivements/1
  # DELETE /memachivements/1.json
  def destroy
    @memachivement.destroy
    respond_to do |format|
      format.html { redirect_to memachivements_url, notice: 'Memachivement was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_memachivement
      @memachivement = Memachivement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def memachivement_params
      params.require(:memachivement).permit(:FullName, :FlatNo, :Phase, :Achivement, :Description, :image)
    end

end

