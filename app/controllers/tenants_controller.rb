class TenantsController < ApplicationController

  def index
  
     @tenants = Tenant.all
  end

  def edit
       @tenant=Tenant.find(params[:id])
  end

  def show
  	@tenant=Tenant.find(params[:id])
  end

  def new
    @tenant=Tenant.new
  end

 def update
    @tenant=Tenant.find(params[:id])
    if @tenant.update(tenant_params)
 
      redirect_to @tenant
    else
      render 'edit'
  end
end

  def create
  	@tenant= Tenant.new(tenant_params)
  	if @tenant.save
  	 redirect_to @tenant
    else
      render 'new'
  end
end
  
private
def tenant_params
      params.require(:tenant).permit(:ownerid,:firstname, :lastname, :email, :landlineno, :mobileno, :dob, :gender, :age, :nooffamily, :profession, :pancardno, :address,:nooftwowheeler,:nooffourwheeler)
    end

end
