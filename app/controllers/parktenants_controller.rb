class ParktenantsController < ApplicationController
 def index
  
     @parktenants = Parktenant.all
  end

  def edit
       @parktenant=Parktenant.find(params[:id])
  end

  def show
  	@parktenant=Parktenant.find(params[:id])
  end

  def new
    @parktenant=Parktenant.new
  end

 def update
    @parktenant=Parktenant.find(params[:id])
    if @parktenant.update(parktenant_params)
 
      redirect_to @parktenant
    else
      render 'edit'
  end
end

  def create
  	@parktenant= Parktenant.new(parktenant_params)
  	if @parktenant.save
  	 redirect_to @parktenant
    else
      render 'new'
  end
end
  
private
def parktenant_params
      params.require(:parktenant).permit(:ownerid,:nooftwowheeler,:nooffourwheeler)
    end
end
