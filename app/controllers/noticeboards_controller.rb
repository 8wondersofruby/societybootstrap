class NoticeboardsController < ApplicationController


  before_action :set_noticeboard, only: [:show, :edit, :update, :destroy]

   def index
    @noticeboards = Noticeboard.all
  end



 def eventnotice
  @noticeboards = Noticeboard.all
   
 end


 def show
 end



  def new
    @noticeboard = Noticeboard.new
  end

  def edit
  end

 def create
        @noticeboard = Noticeboard.new(noticeboard_params)
          
      if @noticeboard.save
        redirect_to noticeboards_path, notice: "The notice #{@noticeboard.title} has been uploded."
        #format.html { redirect_to @noticeboard, notice: 'Noticeboard was successfully created.' }
        
      else
       render :new 
        
      end
    end
    

  def update
    respond_to do |format|
      if @noticeboard.update(noticeboard_params)
        format.html { redirect_to @noticeboard, notice: 'Noticeboard was successfully updated.' }
        
      else
        format.html { render :edit }
        
      end
    end
  end

  
  def destroy
    @noticeboard.destroy
    respond_to do |format|
    format.html { redirect_to noticeboards_url, notice: 'Noticeboard was successfully destroyed.' }
      
    end
  end
 
  private
    
    def set_noticeboard
      @noticeboard = Noticeboard.find(params[:id])
    end
   def noticeboard_params
      params.require(:noticeboard).permit(:title, :description, :validuntil, :attachment)
    end

end

