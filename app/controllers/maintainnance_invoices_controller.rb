class MaintainnanceInvoicesController < ApplicationController
  before_action :set_maintainnance_invoice, only: [:show, :edit, :update, :destroy]

  # GET /maintainnance_invoices
  # GET /maintainnance_invoices.json
  def index
    @maintainnance_invoices = MaintainnanceInvoice.all
  end

  # GET /maintainnance_invoices/1
  # GET /maintainnance_invoices/1.json
  def show
  end

  # GET /maintainnance_invoices/new
  def new
    @maintainnance_invoice = MaintainnanceInvoice.new
  end

  # GET /maintainnance_invoices/1/edit
  def edit
  end

  # POST /maintainnance_invoices
  # POST /maintainnance_invoices.json
  def create
    @maintainnance_invoice = MaintainnanceInvoice.new(maintainnance_invoice_params)

    respond_to do |format|
      if @maintainnance_invoice.save
        format.html { redirect_to @maintainnance_invoice, notice: 'Maintainnance invoice was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /maintainnance_invoices/1
  # PATCH/PUT /maintainnance_invoices/1.json
  def update
    respond_to do |format|
      if @maintainnance_invoice.update(maintainnance_invoice_params)
        format.html { redirect_to @maintainnance_invoice, notice: 'Maintainnance invoice was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /maintainnance_invoices/1
  # DELETE /maintainnance_invoices/1.json
  def destroy
    @maintainnance_invoice.destroy
    respond_to do |format|
      format.html { redirect_to maintainnance_invoices_url, notice: 'Maintainnance invoice was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_maintainnance_invoice
      @maintainnance_invoice = MaintainnanceInvoice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def maintainnance_invoice_params
      params.require(:maintainnance_invoice).permit(:iv_year, :iv_freq, :iv_period, :iv_fromdate, :iv_todate, :invoicedate, :iv_duedate)
    end
end