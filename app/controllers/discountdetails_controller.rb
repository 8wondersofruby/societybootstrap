class DiscountdetailsController < ApplicationController
  before_action :set_discountdetail, only: [:show, :edit, :update, :destroy]

  # GET /discountdetails
  # GET /discountdetails.json
  def index
    @discountdetails = Discountdetail.all
  end

  # GET /discountdetails/1
  # GET /discountdetails/1.json
  def show
  end

  # GET /discountdetails/new
  def new
    @discountdetail = Discountdetail.new
  end

  # GET /discountdetails/1/edit
  def edit
  end

  # POST /discountdetails
  # POST /discountdetails.json
  def create
    @discountdetail = Discountdetail.new(discountdetail_params)

    respond_to do |format|
      if @discountdetail.save
        format.html { redirect_to @discountdetail, notice: 'Discountdetail was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /discountdetails/1
  # PATCH/PUT /discountdetails/1.json
  def update
    respond_to do |format|
      if @discountdetail.update(discountdetail_params)
        format.html { redirect_to @discountdetail, notice: 'Discountdetail was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /discountdetails/1
  # DELETE /discountdetails/1.json
  def destroy
    @discountdetail.destroy
    respond_to do |format|
      format.html { redirect_to discountdetails_url, notice: 'Discountdetail was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_discountdetail
      @discountdetail = Discountdetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def discountdetail_params
      params.require(:discountdetail).permit(:discdesc, :disamount)
    end
end
