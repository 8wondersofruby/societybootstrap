class FlatownersController < ApplicationController
  def index

     @flatowners = Flatowner.all
     @user = User.all
  end

  def edit
    @flatowner=Flatowner.find(params[:id])
  end

  def show
  	@flatowner=Flatowner.find(params[:id])
  end

  def new
     @flatowner =Flatowner.new
  end

  def update
    @flatowner=Flatowner.find(params[:id])
    if @flatowner.update(flatowner_params)
 
      redirect_to @flatowner
    else
      render 'edit'
  end
end

  def create
  	@flatowner= Flatowner.new(flatowner_params)
     
  	if @flatowner.save
  	redirect_to @flatowner
  else
    render 'new'
  end
end

  
private
def flatowner_params
      params.require(:flatowner).permit(:phasename, :subphase, :flatno, :flattype, :flatarea, :user_id)
    end
end
