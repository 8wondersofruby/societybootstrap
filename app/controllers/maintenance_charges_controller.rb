class MaintenanceChargesController < ApplicationController
  before_action :set_maintenance_charge, only: [:show, :edit, :update, :destroy]

  # GET /maintenance_charges
  # GET /maintenance_charges.json
  def index
    @maintenance_charges = MaintenanceCharge.all
  end

  # GET /maintenance_charges/1
  # GET /maintenance_charges/1.json
  def show
  end

  # GET /maintenance_charges/new
  def new
    @maintenance_charge = MaintenanceCharge.new
  end

  # GET /maintenance_charges/1/edit
  def edit
  end

  # POST /maintenance_charges
  # POST /maintenance_charges.json
  def create
    @maintenance_charge = MaintenanceCharge.new(maintenance_charge_params)

    respond_to do |format|
      if @maintenance_charge.save
        format.html { redirect_to @maintenance_charge, notice: 'Maintenance charge was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /maintenance_charges/1
  # PATCH/PUT /maintenance_charges/1.json
  def update
    respond_to do |format|
      if @maintenance_charge.update(maintenance_charge_params)
        format.html { redirect_to @maintenance_charge, notice: 'Maintenance charge was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /maintenance_charges/1
  # DELETE /maintenance_charges/1.json
  def destroy
    @maintenance_charge.destroy
    respond_to do |format|
      format.html { redirect_to maintenance_charges_url, notice: 'Maintenance charge was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_maintenance_charge
      @maintenance_charge = MaintenanceCharge.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def maintenance_charge_params
      params.require(:maintenance_charge).permit(:mnt_baseamount, :mnt_sinkingfnd, :mnt_otherfund, :mnt_odesc)
    end
end
