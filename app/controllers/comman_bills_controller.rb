class CommanBillsController < ApplicationController
  before_action :set_comman_bill, only: [:show, :edit, :update, :destroy]


  def index
    @comman_bills = CommanBill.all
  end


  def show
  end



  def new
    @comman_bill = CommanBill.new
  end


  def edit
  end

  def create
    @comman_bill = CommanBill.new(comman_bill_params)

    respond_to do |format|
      if @comman_bill.save

        format.html { redirect_to comman_bills_path, notice:  "The notice #{@comman_bill.bill_type} has been uploded." }
      else

         format.html { render :new }
      end
    end
end

 def update
    respond_to do |format|
      if @comman_bill.update(comman_bill_params)
        format.html { redirect_to @comman_bill, notice: 'Comman bill was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @comman_bill.destroy
    respond_to do |format|
      format.html { redirect_to comman_bills_url, notice: 'Comman bill was successfully destroyed.' }
    end
  end

  private

    def set_comman_bill
      @comman_bill = CommanBill.find(params[:id])
    end

   
    def comman_bill_params
      params.require(:comman_bill).permit(:bill_type, :date, :due_date, :bill_ammount, :due_ammount, :view)
    end
end
