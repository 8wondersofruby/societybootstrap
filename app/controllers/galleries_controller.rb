class GalleriesController < ApplicationController
  before_action :set_gallery, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @galleries = Gallery.all
    respond_with(@galleries)
  end

  def show
 @galleryattachments = @gallery.galleryattachments.all 
  end

  def new
  @gallery = Gallery.new
   @galleryattachments =  @gallery.galleryattachments.build
  end

  def edit
  end

  def create
    @gallery = Gallery.new(gallery_params)

   respond_to do |format|
     if @gallery.save
       params[:galleryattachments]['avatar'].each do |a|
          @galleryattachment = @gallery.galleryattachments.create!(:avatar => a, :gallery_id => @gallery.id)
       end
       format.html { redirect_to @gallery, notice: 'Post was successfully created.' }
     else
       format.html { render action: 'new' }
     end
   end
  end

  def update
    @gallery.update(gallery_params)
    respond_with(@gallery)
  end

  def destroy
    @gallery.destroy
    respond_with(@gallery)
  end

  private
    def set_gallery
      @gallery = Gallery.find(params[:id])
    end

    def gallery_params
      params.require(:gallery).permit(:title, galleryattachments_attributes: [:id, :gallery_id, :avatar])
    end
end
