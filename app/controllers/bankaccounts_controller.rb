class BankaccountsController < ApplicationController


  before_action :set_bankaccount, only: [:show, :edit, :update, :destroy]


  def index
    @bankaccounts = Bankaccount.all
  end

  
  def show
  end

  
  def new
    @bankaccount = Bankaccount.new
  end

  
  def edit
  end

  
  def create
    @bankaccount = Bankaccount.new(bankaccount_params)

    respond_to do |format|
      if @bankaccount.save
        format.html { redirect_to @bankaccount, notice: 'Bankaccount was successfully created.' }
        
      else
        format.html { render :new }
        
      end
    end
  end

  
  def update
    respond_to do |format|
      if @bankaccount.update(bankaccount_params)
        format.html { redirect_to @bankaccount, notice: 'Bankaccount was successfully updated.' }
        
      else
        format.html { render :edit }
        
      end
    end
  end

  
  def destroy
    @bankaccount.destroy
    respond_to do |format|
      format.html { redirect_to bankaccounts_url, notice: 'Bankaccount was successfully destroyed.' }
      
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bankaccount
      @bankaccount = Bankaccount.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bankaccount_params
      params.require(:bankaccount).permit(:acc_no, :acc_name, :bank_name, :bank_branch, :bank_add, :bank_balance)
    end

end



