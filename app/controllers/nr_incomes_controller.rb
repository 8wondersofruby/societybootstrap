 class NrIncomesController < ApplicationController
  before_action :set_nr_income, only: [:show, :edit, :update, :destroy]

  # GET /nr_incomes
  # GET /nr_incomes.json
  def index
    @nr_incomes = NrIncome.all
  end

  # GET /nr_incomes/1
  # GET /nr_incomes/1.json
  def show
  end

  # GET /nr_incomes/new
  def new
      @nr_income_type = ["Transfer","Cheque","Draft","Cash"]
    @nr_income = NrIncome.new
        @bankaccounts = Bankaccount.all

  end

  # GET /nr_incomes/1/edit
  def edit
  end

  # POST /nr_incomes
  # POST /nr_incomes.json
  def create
    @nr_income = NrIncome.new(nr_income_params)

    respond_to do |format|
      if @nr_income.save
        format.html { redirect_to @nr_income, notice: 'Nr income was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /nr_incomes/1
  # PATCH/PUT /nr_incomes/1.json
  def update
    respond_to do |format|
      if @nr_income.update(nr_income_params)
        format.html { redirect_to @nr_income, notice: 'Nr income was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /nr_incomes/1
  # DELETE /nr_incomes/1.json
  def destroy
    @nr_income.destroy
    respond_to do |format|
      format.html { redirect_to nr_incomes_url, notice: 'Nr income was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nr_income
      @nr_income = NrIncome.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def nr_income_params
      params.require(:nr_income).permit(:nri_type, :bank_account, :member_type, :name, :phase_no, :flat_no, :amount, :date, :chq_desc, :chq_no, :chqbank_name, :chqbranch_name, :chq_date, :trs_desc, :trs_bankname, :trs_no, :trs_date, :draft_des, :draft_no, :draft_bankname, :draft_branchname, :draft_date, :cash_desc, :cash_date, :pay_mode)
    end
end

