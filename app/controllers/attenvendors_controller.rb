class AttenvendorsController < ApplicationController

  def index

     @attenvendors = Attenvendor.all
  end

  def edit
    @attenvendor=Attenvendor.find(params[:id])
  end

  def show
  	@attenvendor=Attenvendor.find(params[:id])
  end

  def new
     @attenvendor = Attenvendor.new
  end

  def update
    @attenvendor=Attenvendor.find(params[:id])
    if @attenvendor.update(attenvendor_params)
 
      redirect_to @attenvendor
    else
      render 'edit'
  end
end

  def create
  	@attenvendor= Attenvendor.new(attenvendor_params)
  	if @attenvendor.save
  	redirect_to @attenvendor
  else
    render 'new'
  end
end

  
private
def attenvendor_params
      params.require(:attenvendor).permit(:firstname, :lastname, :date, :timein, :timeout, :phasename, :vendortype)
    end

end







