class VendorsController < ApplicationController

  

 # before_action :set_vendor, only: [:show, :edit, :update, :destroy]

  # GET /comitteemembers
  # GET /comitteemembers.json
  def index
    @search = Vendor.search do
     fulltext params[:search]
  end
  @vendors=@search.results
end

  # GET /comitteemembers/1
  # GET /comitteemembers/1.json
  def show
  	@vendor= Vendor.find(params[:id])
  end

  
  def new
    @vendor = Vendor.new
    @vendor.save
  end

  def edit
  	@vendor= Vendor.find(params[:id])
  end

  def create
    @vendor = Vendor.new(vendor_params)

    respond_to do |format|
      if @vendor.save
        format.html { redirect_to @vendor, notice: 'vendor was successfully created.' }

      else
        format.html { render :new }

      end
    end
  end

  
  def update
    respond_to do |format|
      if @vendor.update(vendor_params)
        format.html { redirect_to @vendor, notice: 'vendor was successfully updated.' }

      else
        format.html { render :edit }

      end
    end
  end

  
  def destroy
   
     @vendor = Vendor.find(params[:id])
     @vendor.destroy
     redirect_to vendors_path
    end
  

  private
    
    def set_vendor
      @vendor = Vendor.find(params[:id])
    end

    
    def vendor_params
      params.require(:vendor).permit(:fullname, :address, :contactno, :dob, :gender, :vendortype, :contractername, :contracterno, :contractperiod, :contractstartdate, :contractenddate, :panno, :servicetax, :tds, :salary )
    end


end
