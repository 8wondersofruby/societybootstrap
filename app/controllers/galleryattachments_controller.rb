class GalleryattachmentsController < ApplicationController
  before_action :set_galleryattachment, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @galleryattachments = Galleryattachment.all
    respond_with(@galleryattachments)
  end

  def show
    respond_with(@galleryattachment)
  end

  def new
    @galleryattachment = Galleryattachment.new
    respond_with(@galleryattachment)
  end

  def edit
  end

  def create
    @galleryattachment = Galleryattachment.new(galleryattachment_params)
    @galleryattachment.save
    respond_with(@galleryattachment)
  end

 def update
  respond_to do |format|
    if @galleryattachment.update(galleryattachment_params)
      format.html { redirect_to @galleryattachment.gallery, notice: 'Post attachment was successfully updated.' }
    end 
  end
end

  def destroy
    @galleryattachment.destroy
    respond_with(@galleryattachment)
  end

  private
    def set_galleryattachment
      @galleryattachment = Galleryattachment.find(params[:id])
    end

    def galleryattachment_params
      params.require(:galleryattachment).permit(:gallery_id, :avatar)
    end
end
