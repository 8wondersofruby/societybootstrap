class AttenvisitorsController < ApplicationController
  def index

     @attenvisitors = Attenvisitor.all
  end

  def edit
    @attenvisitor=Attenvisitor.find(params[:id])
  end

  def show
  	@attenvisitor=Attenvisitor.find(params[:id])
  end

  def new
     @attenvisitor = Attenvisitor.new
  end

  def update
    @attenvisitor=Attenvisitor.find(params[:id])
    if @attenvisitor.update(attenvisitor_params)
 
      redirect_to @attenvisitor
    else
      render 'edit'
  end
end

  def create
  	@attenvisitor= Attenvisitor.new(attenvisitor_params)
  	if @attenvisitor.save
  	redirect_to @attenvisitor
  else
    render 'new'
  end
end

  
private
def attenvisitor_params
      params.require(:attenvisitor).permit(:firstname, :lastname, :date, :timein, :timeout, :phasename, :flatno, :mobileno, :address)
    end

end






