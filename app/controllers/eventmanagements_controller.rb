class EventmanagementsController < ApplicationController
def index
    @eventmanagements = Eventmanagement.all
    @eventmanagements_by_date = @eventmanagements.group_by(&:EventStartDate)
    @date= params[:date] ? Date.parse(params[:date]) : Date.today
  end
  def show
    @eventmanagement=Eventmanagement.find(params[:id])
  end
  def new
    @eventmanagement = Eventmanagement.new
  end
  def edit

  end
 def create
    @eventmanagement = Eventmanagement.new(eventmanagement_params)

    respond_to do |format|
      if @eventmanagement.save
        format.html { redirect_to @eventmanagement, notice: 'Eventmanagement was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  
  def update
    respond_to do |format|
      if @eventmanagement.update(eventmanagement_params)
        format.html { redirect_to @eventmanagement, notice: 'Eventmanagement was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end
 def destroy
    @eventmanagement.destroy
    respond_to do |format|
      format.html { redirect_to eventmanagements_url, notice: 'Eventmanagement was successfully destroyed.' }
    end
  end

  private
    def set_eventmanagement
      @eventmanagement = Eventmanagement.find(params[:id])
    end

    def eventmanagement_params
      params.require(:eventmanagement).permit(:EventName, :EventCoordinator, :Email, :EventDescription, :EventType, :Does_Your_Program_involve_Any_Type_Of_Outside_Activity, :Requirements, :EventStartDate, :EventEndDate, :Location, :city, :image)
    end
end



