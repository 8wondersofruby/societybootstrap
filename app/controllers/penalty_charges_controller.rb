class PenaltyChargesController < ApplicationController
  before_action :set_penalty_charge, only: [:show, :edit, :update, :destroy]

  # GET /penalty_charges
  # GET /penalty_charges.json
  def index
    @penalty_charges = PenaltyCharge.all
  end

  # GET /penalty_charges/1
  # GET /penalty_charges/1.json
  def show
  end

  # GET /penalty_charges/new
  def new
    @penalty_charge = PenaltyCharge.new
  end

  # GET /penalty_charges/1/edit
  def edit
  end

  # POST /penalty_charges
  # POST /penalty_charges.json
  def create
    @penalty_charge = PenaltyCharge.new(penalty_charge_params)

    respond_to do |format|
      if @penalty_charge.save
        format.html { redirect_to @penalty_charge, notice: 'Penalty charge was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /penalty_charges/1
  # PATCH/PUT /penalty_charges/1.json
  def update
    respond_to do |format|
      if @penalty_charge.update(penalty_charge_params)
        format.html { redirect_to @penalty_charge, notice: 'Penalty charge was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /penalty_charges/1
  # DELETE /penalty_charges/1.json
  def destroy
    @penalty_charge.destroy
    respond_to do |format|
      format.html { redirect_to penalty_charges_url, notice: 'Penalty charge was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_penalty_charge
      @penalty_charge = PenaltyCharge.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def penalty_charge_params
      params.require(:penalty_charge).permit(:pn_desc, :pn_ovrdueamt, :pn_ovrduedays)
    end
end