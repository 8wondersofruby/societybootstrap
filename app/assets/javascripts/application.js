// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap-sprockets
//= require_tree .



//= require  bootstrap.js
//= require  bootstrap.min.js

//= require  lightbox.js
//= require  lightbox.min.js
//= require  showhide.js
//= require  jquery.min.js
//= require  modernizr.custom.js
//= require  Gruntfile.js
//= require  jquery.gridster.js
//= require  jquery.gridster.min.js
//= require  jquery.gridster.with-extras.js
//= require  jquery.gridster.with-extras.min.js
//= require  jquery.collision.js
//= require  jquery.coords.js
//= require  jquery.draggable.js
//= require  jquery.gridster.extras.js
//= require  jquery.gridster.js
//= require  utils.js
//= require  jquery.gridder_test.js


//= require  Gruntfile.js
//= require  jquery.gridster.js
//= require  jquery.gridster.min.js
//= require  jquery.gridster.with-extras.js
//= require  jquery.gridster.with-extras.min.js
//= require  jquery.collision.js
//= require  jquery.coords.js
//= require  jquery.draggable.js
//= require  jquery.gridster.extras.js
//= require  jquery.gridster.js
//= require  utils.js
//= require  jquery.gridder_test.js
//= require fullcalendar



//= require jquery.gridster.js
//= require Gruntfile.js
//= require jquery.gridster.min.js
//= require jquery.gridster.with-extras.js
//= require jquery.gridster.with-extras.min.js
//= require jquery.collision.js
//= require jquery.coords.js
//= require jquery.draggable.js
//= require jquery.gridster.extras.js
//= require jquery.gridster.js
//= require utils.js
//= require jquery.gridder_test.js


$(function() {
    $('#search').on('keyup', function() {
        var pattern = $(this).val();
        $('.searchable-container .items').hide();
        $('.searchable-container .items').filter(function() {
            return $(this).text().match(new RegExp(pattern, 'i'));
        }).show();
    });
});

