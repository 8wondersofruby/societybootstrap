json.array!(@galleryattachments) do |galleryattachment|
  json.extract! galleryattachment, :id, :gallery_id, :avatar
  json.url galleryattachment_url(galleryattachment, format: :json)
end
