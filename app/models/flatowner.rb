# == Schema Information
#
# Table name: flatowners
#
#  id         :integer          not null, primary key
#  phasename  :string(255)
#  subphase   :string(255)
#  flatno     :integer
#  flattype   :string(255)
#  flatarea   :float
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#

class Flatowner < ActiveRecord::Base
	belongs_to :regowners
	belongs_to :users
end
