# == Schema Information
#
# Table name: bankaccounts
#
#  id           :integer          not null, primary key
#  acc_no       :string(255)
#  acc_name     :string(255)
#  bank_name    :string(255)
#  bank_branch  :string(255)
#  bank_add     :text
#  bank_balance :float
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Bankaccount < ActiveRecord::Base
	validates :acc_no, presence: true, length: {:in => 3..16}


	belongs_to :nr_income


	
   	validates :acc_name, presence: true,
   			   length: { minimum: 3 }
   	validates_format_of :acc_name, :with => /([a-z]|[A-Z])/


	validates :bank_name, presence: true
	validates :bank_branch, presence: true
	validates :bank_add, presence: true,
			   length: { in: 5..50 }   
	validates :bank_balance, presence: true


end
