# == Schema Information
#
# Table name: comitteemembers
#
#  id             :integer          not null, primary key
#  fullname       :string(255)
#  gender         :string(255)
#  role           :string(255)
#  email          :string(255)
#  phaseno        :string(255)
#  responsiblefor :text
#  startdate      :date
#  duration       :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Comitteemember < ActiveRecord::Base
end
