# == Schema Information
#
# Table name: attenvisitors
#
#  id         :integer          not null, primary key
#  firstname  :string(255)
#  lastname   :string(255)
#  date       :date
#  timein     :time
#  timeout    :time
#  phasename  :string(255)
#  flatno     :integer
#  mobileno   :string(255)
#  address    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Attenvisitor < ActiveRecord::Base
end
