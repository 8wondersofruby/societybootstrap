# == Schema Information
#
# Table name: nr_incomes
#
#  id               :integer          not null, primary key
#  nri_type         :string(255)
#  bank_account     :integer
#  member_type      :string(255)
#  name             :string(255)
#  phase_no         :integer
#  flat_no          :integer
#  amount           :float
#  date             :date
#  chq_desc         :text
#  chq_no           :string(255)
#  chqbank_name     :string(255)
#  chqbranch_name   :string(255)
#  chq_date         :date
#  trs_desc         :text
#  trs_bankname     :string(255)
#  trs_no           :string(255)
#  trs_date         :date
#  draft_des        :text
#  draft_no         :string(255)
#  draft_bankname   :string(255)
#  draft_branchname :string(255)
#  draft_date       :date
#  cash_desc        :text
#  cash_date        :date
#  pay_mode         :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class NrIncome < ActiveRecord::Base
	has_many :bankaccounts
end
