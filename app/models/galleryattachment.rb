# == Schema Information
#
# Table name: galleryattachments
#
#  id            :integer          not null, primary key
#  gallery_id :integer
#  avatar        :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#

class Galleryattachment < ActiveRecord::Base
mount_uploader :avatar, AvatarUploader
   belongs_to :gallery
end
