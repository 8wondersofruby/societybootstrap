# == Schema Information
#
# Table name: noticeboards
#
#  id          :integer          not null, primary key
#  title       :string(255)
#  description :text
#  validuntil  :date
#  attachment  :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Noticeboard < ActiveRecord::Base
	mount_uploader :attachment, AttachmentUploader
	validates :title, :description,:validuntil, presence: true
end
