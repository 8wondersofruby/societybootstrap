# == Schema Information
#
# Table name: regowners
#
#  id                 :integer          not null, primary key
#  firstname          :string(255)
#  lastname           :string(255)
#  email              :string(255)
#  landlineno         :string(255)
#  mobileno           :string(255)
#  dob                :date
#  gender             :string(255)
#  age                :integer
#  nooffamily         :integer
#  profession         :string(255)
#  pancardno          :string(255)
#  address            :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  flatowner_id       :integer
#

#  id                 :integer          not null, primary key
#  firstname          :string
#  lastname           :string
#  email              :string
#  landlineno         :string
#  mobileno           :string
#  dob                :date
#  gender             :string
#  age                :integer
#  nooffamily         :integer
#  profession         :string
#  pancardno          :string
#  address            :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#

class Regowner < ActiveRecord::Base

	has_many :parkowner
	has_many :flatowners

	belongs_to :users
	has_attached_file :image, styles: {large:"600*600>",medium:"300*300>", thumb:"150*150#"}
     validates_attachment_content_type :image, content_type:/\Aimage\/.*\z/
      searchable do
    text :firstname
end


end



