# == Schema Information
#
# Table name: galleries
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Gallery < ActiveRecord::Base
 has_many :galleryattachments
   accepts_nested_attributes_for :galleryattachments
end
