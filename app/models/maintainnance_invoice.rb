# == Schema Information
#
# Table name: maintainnance_invoices
#
#  id          :integer          not null, primary key
#  iv_year     :string(255)
#  iv_freq     :string(255)
#  iv_period   :string(255)
#  iv_fromdate :date
#  iv_todate   :date
#  invoicedate :date
#  iv_duedate  :date
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class MaintainnanceInvoice < ActiveRecord::Base
end
