# == Schema Information
#
# Table name: eventmanagements
#
#  id                                                     :integer          not null, primary key
#  EventName                                              :string(255)
#  EventCoordinator                                       :string(255)
#  Email                                                  :string(255)
#  EventDescription                                       :text
#  EventType                                              :string(255)
#  Does_Your_Program_involve_Any_Type_Of_Outside_Activity :string(255)
#  Requirements                                           :string(255)
#  EventStartDate                                         :datetime
#  EventEndDate                                           :datetime
#  Location                                               :text
#  city                                                   :string(255)
#  created_at                                             :datetime         not null
#  updated_at                                             :datetime         not null
#  image_file_name                                        :string(255)
#  image_content_type                                     :string(255)
#  image_file_size                                        :integer
#  image_updated_at                                       :datetime
#

class Eventmanagement < ActiveRecord::Base


has_attached_file :image, styles: {large:"600*600>",medium:"300*300>", thumb:"150*150#"}
     validates_attachment_content_type :image, content_type:/\Aimage\/.*\z/
     
 # validates_format_of :EventName, :with => /\A[^0-9`!@#\$%\^&*+_=]+\z/ 
                   

 #  validates_format_of :EventCoordinator, :with => /\A[^0-9`!@#\$%\^&*+_=]+\z/                     
  
 # validates :EventType , presence: true

                       
 #  validates :Email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }                 										


 #  validates  :EventDescription , presence: true,
 #                    length: { in: 5..50 }
              
 #  validates_format_of :Location, :with => /\A[^0-9`!@#\$%\^&*+_=]+\z/ 
  
  
 #  validates_format_of :city, :with => /\A[^0-9`!@#\$%\^&*+_=]+\z/ 
  
end
