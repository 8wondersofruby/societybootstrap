# == Schema Information
#
# Table name: comman_bills
#
#  id           :integer          not null, primary key
#  bill_type    :string(255)
#  date         :date
#  due_date     :date
#  bill_ammount :float
#  due_ammount  :float
#  view         :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class CommanBill < ActiveRecord::Base

mount_uploader :view, ViewUploader


end

