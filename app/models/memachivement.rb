# == Schema Information
#
# Table name: memachivements
#
#  id                 :integer          not null, primary key
#  FullName           :string(255)
#  Achivement         :string(255)
#  Description        :text
#  FlatNo             :integer
#  Phase              :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#

class Memachivement < ActiveRecord::Base

   has_attached_file :image, styles: {large:"600*600>",medium:"300*300>", thumb:"150*150#"}
     validates_attachment_content_type :image, content_type:/\Aimage\/.*\z/
   validates_format_of :FullName, :with => /\A[^0-9`!@#\$%\^&*+_=]+\z/               


  validates :FlatNo , presence: true


  validates :Phase , presence: true

  validates  :Achivement , presence: true,
                    length: { in: 5..30 }                  

  validates  :Description , presence: true,
                    length: { in: 10..50 }
                  

  

end
