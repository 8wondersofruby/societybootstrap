# == Schema Information
#
# Table name: maintenance_charges
#
#  id             :integer          not null, primary key
#  mnt_baseamount :float
#  mnt_sinkingfnd :float
#  mnt_otherfund  :float
#  mnt_odesc      :text
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class MaintenanceCharge < ActiveRecord::Base
end
