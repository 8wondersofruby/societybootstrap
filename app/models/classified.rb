# == Schema Information
#
# Table name: classifieds
#
#  id         :integer          not null, primary key
#  adtitle    :string(255)
#  addesc     :text
#  sale       :string(255)
#  category   :string(255)
#  prize      :float
#  contactno  :string(255)
#  validuntil :string(255)
#  visibility :string(255)
#  created_at :datetime
#  updated_at :datetime
#


class Classified < ActiveRecord::Base
	has_attached_file :image, styles: {large:"600*600>",medium:"300*300>", thumb:"150*150#"}
     validates_attachment_content_type :image, content_type:/\Aimage\/.*\z/
     

end
