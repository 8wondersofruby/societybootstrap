# == Schema Information
#
# Table name: parkowners
#
#  id              :integer          not null, primary key
#  parkingno       :integer
#  nooftwowheeler  :integer
#  nooffourwheeler :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Parkowner < ActiveRecord::Base
	belongs_to :regowner
end
