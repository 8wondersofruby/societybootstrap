# == Schema Information
#
# Table name: meetings
#
#  id              :integer          not null, primary key
#  phase           :string(255)
#  pointsdiscussed :text
#  dateandtime     :datetime
#  venue           :string(255)
#  conclusion      :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Meeting < ActiveRecord::Base
end
