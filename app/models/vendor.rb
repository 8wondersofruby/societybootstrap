# == Schema Information
#
# Table name: vendors
#
#  id                :integer          not null, primary key
#  fullname          :string(255)
#  address           :text
#  contactno         :integer
#  dob               :date
#  gender            :string(255)
#  vendortype        :string(255)
#  contracterno      :string(255)
#  contractperiod    :integer
#  contractstartdate :date
#  contractenddate   :date
#  panno             :string(255)
#  servicetax        :float
#  tds               :float
#  salary            :float
#  contractername    :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Vendor < ActiveRecord::Base
	searchable do
	text :fullname
end
end
