# == Schema Information
#
# Table name: regtenants
#
#  id                 :integer          not null, primary key
#  ownerid            :integer
#  firstname          :string(255)
#  lastname           :string(255)
#  landlineno         :string(255)
#  mobileno           :string(255)
#  dob                :date
#  age                :integer
#  gender             :string(255)
#  nooffamily         :integer
#  profession         :string(255)
#  pancardno          :string(255)
#  address            :text
#  email              :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#

class Regtenant < ActiveRecord::Base
	has_attached_file :image, styles: {large:"600*600>",medium:"300*300>", thumb:"150*150#"}
     validates_attachment_content_type :image, content_type:/\Aimage\/.*\z/
end
