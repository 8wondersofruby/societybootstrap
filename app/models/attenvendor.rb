# == Schema Information
#
# Table name: attenvendors
#
#  id         :integer          not null, primary key
#  firstname  :string(255)
#  lastname   :string(255)
#  date       :date
#  timein     :time
#  timeout    :time
#  vendortype :string(255)
#  phasename  :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Attenvendor < ActiveRecord::Base
end
