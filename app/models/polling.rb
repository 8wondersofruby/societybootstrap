# == Schema Information
#
# Table name: pollings
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  email_id           :string(255)
#  wing               :string(255)
#  flatno             :integer
#  age                :integer
#  positiontype       :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#

class Polling < ActiveRecord::Base
	has_attached_file :image, styles: {large:"600*600>",medium:"300*300>", thumb:"150*150#"}
     validates_attachment_content_type :image, content_type:/\Aimage\/.*\z/
     

     acts_as_votable
end
 
