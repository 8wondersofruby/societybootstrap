Rails.application.routes.draw do

 



  get 'new/index'

  
  



  get 'rules/new'

  get 'rules/index'

  get 'rules/edit'

  get 'rules/show'


  devise_for :admins
  get 'documents/index'

  get 'documents/new'

  get 'documents/show'

  get 'documents/edit'

  get 'comman_bills/index'

  get 'comman_bills/new'

  get 'comman_bills/edit'

  get 'comman_bills/show'


  get 'nr_incomes/index'

  get 'nr_incomes/show'

  get 'nr_incomes/edit'

  get 'nr_incomes/new'
  get 'acc_payments/index'

  get 'acc_payments/new'

  get 'acc_payments/show'

  get 'acc_payments/edit'



  get 'penalty_charges/index'

  get 'penalty_charges/new'

  get 'penalty_charges/edit'

  get 'penalty_charges/show'




  get 'posts/index'
  devise_for :users, :controllers => { omniauth_callbacks: 'omniauth_callbacks' }

scope "/admin" do
  resources :users
end
  get 'maintainnance_invoices/index'

  get 'maintainnance_invoices/new'

  get 'maintainnance_invoices/edit'

  get 'maintainnance_invoices/show'
  
  get 'penalty_charges/index'

  get 'penalty_charges/new'

  get 'penalty_charges/edit'

  get 'penalty_charges/show'

  get 'attenvendors/index'

  get 'classifieds/index'

  get 'classifieds/edit'


  get 'noticeboards/display'

  get 'noticeboards/new'

  get 'classifieds/show'


  get 'classifieds/new'
  get 'attendances/index'

  get 'attendances/edit'

  get 'attendances/show'

  get 'attendances/new'

  get 'discountdetails/index'

  get 'discountdetails/edit'

  get 'discountdetails/show'

  get 'discountdetails/new'
  
  get 'visitors/services'
  get 'visitors/adminindex'
  get 'visitors/accountingindex'
  get 'visitors/eventnotice'

  get 'visitors/account'
  


  get 'visitors/features'
  get 'regowners/index'
 

   root to: 'visitors#index'
   #root to: 'users#index'


  get 'users/index'
  
 
  get 'eventnotice' => 'visitors#eventnotice'
  get 'instantpol' => 'visitors#instantpol'
  get 'membersearch' => 'visitors#membersearch'
 get 'visitors/upcomming_meeting'
get 'visitors/accountingindex'
get 'visitors/adminindex' 

   resources :pollings do
    member do
      put "like" => "pollings#upvote"
      put "unlike" => "pollings#downvote"
    end
  end


  resources :classifieds
  resources :rules

  resources :comitteemembers
  resources :meetings
  resources :noticeboards
  resources :vendors

  resources :maintenance_charges
  resources :memachivements

  resources :comman_bills

   resources :bankaccounts

   resources :attenvisitors
   resources :attenvendors
   resources :complaints


   resources :visitors

  resources :regtenants
  resources :parktenants
  resources :parkowners
  resources :regowners
  resources :flatowners
  resources :documents

  resources :maintainnance_invoices
  resources :penalty_charges

  resources :acc_payments
  resources :discountdetails


  resources :nr_incomes
resources :galleries
  resources :events
  resources :items

  resources :roles
  resources :galleryattachments
  resources :users
  resources :classifieds
  resources :posts do
    resources :comments
  end




end
