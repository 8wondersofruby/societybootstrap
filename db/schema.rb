# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150416085522) do

  create_table "acc_payments", force: true do |t|
    t.text     "chq_desc"
    t.string   "chq_no"
    t.string   "bnk_name"
    t.string   "bnk_branch"
    t.date     "chq_date"
    t.text     "cash_desc"
    t.date     "paym_date"
    t.text     "drft_desc"
    t.string   "drft_no"
    t.string   "dbnk_name"
    t.string   "dbnk_branch"
    t.date     "drft_date"
    t.text     "ot_desc"
    t.string   "otbnk_name"
    t.string   "trn_no"
    t.date     "ot_date"
    t.string   "paymode"
    t.date     "exbill_date"
    t.text     "exbill_desc"
    t.float    "exbill_amount"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "admins", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true

  create_table "attenvendors", force: true do |t|


    t.string   "firstname",  limit: nil
    t.string   "lastname",   limit: nil
    t.date     "date"
    t.time     "timein"
    t.time     "timeout"
    t.string   "vendortype"
    t.string   "phasename"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "attenvisitors", force: true do |t|
    t.string   "firstname",  limit: nil
    t.string   "lastname",   limit: nil
    t.date     "date"
    t.time     "timein"
    t.time     "timeout"
    t.string   "phasename"
    t.integer  "flatno"
    t.string   "mobileno"
    t.text     "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bankaccounts", force: true do |t|

    t.integer  "acc_no"
    t.string   "acc_name",     limit: nil
    t.string   "bank_name",    limit: nil
    t.string   "bank_branch",  limit: nil
    t.text     "bank_add"
    t.float    "bank_balance"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "classifieds", force: true do |t|
    t.string   "adtitle"
    t.text     "addesc"
    t.string   "sale"
    t.string   "category"
    t.float    "prize"
    t.string   "contactno"
    t.string   "validuntil"
    t.string   "visibility"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "comitteemembers", force: true do |t|
    t.string   "fullname",       limit: nil
    t.string   "gender",         limit: nil
    t.string   "role",           limit: nil
    t.string   "email",          limit: nil
    t.string   "phaseno",        limit: nil
    t.text     "responsiblefor"
    t.date     "startdate"
    t.integer  "duration"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "comman_bills", force: true do |t|

    t.string   "bill_type",    limit: nil
    t.date     "date"
    t.date     "due_date"
    t.float    "bill_ammount"
    t.float    "due_ammount"
    t.string   "view",         limit: nil
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end


  create_table "comments", force: true do |t|
    t.string   "name"
    t.text     "body"
    t.integer  "post_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comments", ["post_id"], name: "index_comments_on_post_id"

  create_table "complaints", force: true do |t|
    t.string   "subject"

    t.text     "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "discountdetails", force: true do |t|
    t.text     "discdesc"
    t.float    "disamount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "documents", force: true do |t|
    t.string   "title"
    t.string   "attachment"
    t.datetime "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "eventmanagements", force: true do |t|

    t.string   "EventName",                                              limit: nil
    t.string   "EventCoordinator",                                       limit: nil
    t.string   "Email",                                                  limit: nil

    t.text     "EventDescription"
    t.string   "EventType",                                              limit: nil
    t.string   "Does_Your_Program_involve_Any_Type_Of_Outside_Activity", limit: nil
    t.string   "Requirements",                                           limit: nil
    t.datetime "EventStartDate"
    t.datetime "EventEndDate"
    t.text     "Location"
    t.string   "city",                                                   limit: nil
    t.datetime "created_at",                                                         null: false
    t.datetime "updated_at",                                                         null: false
    t.string   "image_file_name",                                        limit: nil
    t.string   "image_content_type",                                     limit: nil
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "events", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.string   "city"
    t.string   "cordinator"
    t.string   "location"
    t.string   "requirment"
    t.string   "outside"
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "flatowners", force: true do |t|
    t.string   "phasename"
    t.string   "subphase"
    t.integer  "flatno"
    t.string   "flattype"
    t.float    "flatarea"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "galleries", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "galleryattachments", force: true do |t|
    t.integer  "gallery_id"
    t.string   "avatar"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "identities", force: true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "identities", ["user_id"], name: "index_identities_on_user_id"

  create_table "items", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.decimal  "price",       precision: 5, scale: 2
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "items", ["user_id"], name: "index_items_on_user_id"

  create_table "maintainnance_invoices", force: true do |t|
    t.string   "iv_year"
    t.string   "iv_freq"
    t.string   "iv_period"
    t.date     "iv_fromdate"
    t.date     "iv_todate"
    t.date     "invoicedate"
    t.date     "iv_duedate"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "maintenance_charges", force: true do |t|
    t.float    "mnt_baseamount"
    t.float    "mnt_sinkingfnd"
    t.float    "mnt_otherfund"
    t.text     "mnt_odesc"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "meetings", force: true do |t|

    t.string   "phase",           limit: nil

    t.text     "pointsdiscussed"
    t.datetime "dateandtime"
    t.string   "venue"
    t.text     "conclusion"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "memachivements", force: true do |t|
    t.string   "FullName",           limit: nil
    t.string   "Achivement",         limit: nil
    t.text     "Description"
    t.integer  "FlatNo"
    t.string   "Phase",              limit: nil
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "image_file_name",    limit: nil
    t.string   "image_content_type", limit: nil
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "noticeboards", force: true do |t|
    t.string   "title",       limit: nil
    t.text     "description"
    t.date     "validuntil"
    t.string   "attachment"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "nr_incomes", force: true do |t|

    t.string   "nri_type",         limit: nil
    t.string   "bank_account",     limit: nil
    t.string   "member_type",      limit: nil
    t.string   "name",             limit: nil
    t.integer  "phase_no"
    t.integer  "flat_no"
    t.float    "amount"
    t.date     "date"
    t.text     "chq_desc"
    t.string   "chq_no"
    t.string   "chqbank_name"
    t.string   "chqbranch_name"
    t.date     "chq_date"
    t.text     "trs_desc"
    t.string   "trs_bankname"
    t.string   "trs_no"
    t.date     "trs_date"
    t.text     "draft_des"
    t.string   "draft_no"
    t.string   "draft_bankname"
    t.string   "draft_branchname"
    t.date     "draft_date"
    t.text     "cash_desc"
    t.date     "cash_date"
    t.string   "pay_mode"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "parkowners", force: true do |t|
    t.integer  "parkingno"
    t.integer  "nooftwowheeler"
    t.integer  "nooffourwheeler"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "parktenants", force: true do |t|
    t.integer  "nooftwowheeler"
    t.integer  "nooffourwheeler"
    t.integer  "ownerid"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "penalty_charges", force: true do |t|
    t.text     "pn_desc"
    t.float    "pn_ovrdueamt"
    t.integer  "pn_ovrduedays"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "pollings", force: true do |t|
    t.string   "name",               limit: nil
    t.string   "email_id",           limit: nil
    t.string   "wing",               limit: nil
    t.integer  "flatno"
    t.integer  "age"
    t.string   "positiontype",       limit: nil
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "image_file_name",    limit: nil
    t.string   "image_content_type", limit: nil
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "posts", force: true do |t|
    t.string   "title"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "regowners", force: true do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "email"
    t.string   "landlineno"
    t.string   "mobileno"
    t.date     "dob"
    t.string   "gender"
    t.integer  "age"
    t.integer  "nooffamily"
    t.string   "profession"
    t.string   "pancardno"
    t.text     "address"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "flatowner_id"
  end

  create_table "regtenants", force: true do |t|
    t.integer  "ownerid"
    t.string   "firstname"
    t.string   "lastname"
    t.string   "landlineno"
    t.string   "mobileno"
    t.date     "dob"
    t.integer  "age"
    t.string   "gender"
    t.integer  "nooffamily"
    t.string   "profession"
    t.string   "pancardno"
    t.text     "address"
    t.string   "email"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "roles", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rules", force: true do |t|
    t.integer  "rulenumber"
    t.text     "rule"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: true do |t|

    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "name"
    t.string   "provider"
    t.string   "uid"
    t.boolean  "admin",                  default: false
    t.integer  "role_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  add_index "users", ["role_id"], name: "index_users_on_role_id"

  create_table "vendors", force: true do |t|
    t.string   "fullname",          limit: nil    t.string   "fullname"
    t.text     "address"
    t.integer  "contactno"
    t.date     "dob"
    t.string   "gender"
    t.string   "vendortype"
    t.string   "contracterno"
    t.integer  "contractperiod"
    t.date     "contractstartdate"
    t.date     "contractenddate"
    t.string   "panno"
    t.float    "servicetax"
    t.float    "tds"
    t.float    "salary"
    t.string   "contractername",    limit: nil
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "votes", force: true do |t|
    t.integer  "votable_id"
    t.string   "votable_type", limit: nil
    t.integer  "voter_id"
    t.string   "voter_type",   limit: nil
    t.boolean  "vote_flag"
    t.string   "vote_scope",   limit: nil
    t.integer  "vote_weight"
    t.datetime "created_at"
    t.datetime "updated_at"

    t.string   "contractername"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

end
