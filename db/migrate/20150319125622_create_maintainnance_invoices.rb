class CreateMaintainnanceInvoices < ActiveRecord::Migration
  def change
    create_table :maintainnance_invoices do |t|
      t.string :iv_year
      t.string :iv_freq
      t.string :iv_period
      t.date :iv_fromdate
      t.date :iv_todate
      t.date :invoicedate
      t.date :iv_duedate

      t.timestamps null: false
    end
  end
end
