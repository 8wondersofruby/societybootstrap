class CreateMemachivements < ActiveRecord::Migration
  def change
    create_table :memachivements do |t|
      t.string :FullName
      t.string :Achivement
      t.text :Description
      t.integer :FlatNo
      t.string :Phase

      t.timestamps null: false
    end
  end
end
