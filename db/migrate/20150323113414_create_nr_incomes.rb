class CreateNrIncomes < ActiveRecord::Migration
  def change
    create_table :nr_incomes do |t|
      t.string :nri_type
      t.integer :bank_account
      t.string :member_type
      t.string :name
      t.integer :phase_no
      t.integer :flat_no
      t.float :amount
      t.date :date
      t.text :chq_desc
      t.string :chq_no
      t.string :chqbank_name
      t.string :chqbranch_name
      t.date :chq_date
      t.text :trs_desc
      t.string :trs_bankname
      t.string :trs_no
      t.date :trs_date
      t.text :draft_des
      t.string :draft_no
      t.string :draft_bankname
      t.string :draft_branchname
      t.date :draft_date
      t.text :cash_desc
      t.date :cash_date
      t.string :pay_mode

      t.timestamps null: false
    end
  end
end
