class CreateRules < ActiveRecord::Migration
  def change
    create_table :rules do |t|
      t.integer :rulenumber
      t.text :rule

      t.timestamps null: false
    end
  end
end