class AddFlatownerIdToRegowner < ActiveRecord::Migration
  def change
    add_column :regowners, :flatowner_id, :integer
  end
end
