class CreateFlatowners < ActiveRecord::Migration
  def change
    create_table :flatowners do |t|
      t.string :phasename
      t.string :subphase
      t.integer :flatno
      t.string :flattype
      t.float :flatarea

      t.timestamps null: false
    end
  end
end
