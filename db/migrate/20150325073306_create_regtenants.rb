class CreateRegtenants < ActiveRecord::Migration
  def change
    create_table :regtenants do |t|
      t.integer :ownerid
      t.string :firstname
      t.string :lastname
      t.string :landlineno
      t.string :mobileno
      t.date :dob
      t.integer :age
      t.string :gender
      t.integer :nooffamily
      t.string :profession
      t.string :pancardno
      t.text :address
      t.string :email

      t.timestamps null: false
    end
  end
end
