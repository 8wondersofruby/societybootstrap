class CreatePenaltyCharges < ActiveRecord::Migration
  def change
    create_table :penalty_charges do |t|
      t.text :pn_desc
      t.float :pn_ovrdueamt
      t.integer :pn_ovrduedays

      t.timestamps null: false
    end
  end
end
