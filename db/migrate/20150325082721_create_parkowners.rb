class CreateParkowners < ActiveRecord::Migration
  def change
    create_table :parkowners do |t|
      t.integer :parkingno
      t.integer :nooftwowheeler
      t.integer :nooffourwheeler

      t.timestamps null: false
    end
  end
end
