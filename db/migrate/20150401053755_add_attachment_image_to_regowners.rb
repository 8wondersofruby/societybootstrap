class AddAttachmentImageToRegowners < ActiveRecord::Migration
  def self.up
    change_table :regowners do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :regowners, :image
  end
end
