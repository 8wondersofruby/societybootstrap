class CreateDiscountdetails < ActiveRecord::Migration
  def change
    create_table :discountdetails do |t|
      t.text :discdesc
      t.float :disamount

      t.timestamps null: false
    end
  end
end
