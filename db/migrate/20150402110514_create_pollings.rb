class CreatePollings < ActiveRecord::Migration
  def change
    create_table :pollings do |t|
      t.string :name
      t.string :email_id
      t.string :wing
      t.integer :flatno
      t.integer :age
      t.string :positiontype

      t.timestamps null: false
    end
  end
end
