class AddAttachmentImageToMemachivements < ActiveRecord::Migration
  def self.up
    change_table :memachivements do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :memachivements, :image
  end
end
