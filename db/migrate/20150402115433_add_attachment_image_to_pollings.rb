class AddAttachmentImageToPollings < ActiveRecord::Migration
  def self.up
    change_table :pollings do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :pollings, :image
  end
end
