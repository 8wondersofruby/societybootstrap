class CreateComitteemembers < ActiveRecord::Migration
  def change
    create_table :comitteemembers do |t|
      t.string :fullname
      t.string :gender
      t.string :role
      t.string :email
      t.string :phaseno
      t.text :responsiblefor
      t.date :startdate
      t.integer :duration

      t.timestamps null: false
    end
  end
end
