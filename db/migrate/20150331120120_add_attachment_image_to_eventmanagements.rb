class AddAttachmentImageToEventmanagements < ActiveRecord::Migration
  def self.up
    change_table :eventmanagements do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :eventmanagements, :image
  end
end
