class CreateAccPayments < ActiveRecord::Migration
  def change
    create_table :acc_payments do |t|
      t.text :chq_desc
      t.string :chq_no
      t.string :bnk_name
      t.string :bnk_branch
      t.date :chq_date
      t.text :cash_desc
      t.date :paym_date
      t.text :drft_desc
      t.string :drft_no
      t.string :dbnk_name
      t.string :dbnk_branch
      t.date :drft_date
      t.text :ot_desc
      t.string :otbnk_name
      t.string :trn_no
      t.date :ot_date
      t.string :paymode
      t.date :exbill_date
      t.text :exbill_desc
      t.float :exbill_amount

      t.timestamps null: false
    end
  end
end
