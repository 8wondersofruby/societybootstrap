class AddAttachmentImageToRegtenants < ActiveRecord::Migration
  def self.up
    change_table :regtenants do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :regtenants, :image
  end
end
