class CreateVendors < ActiveRecord::Migration
  def change
    create_table :vendors do |t|
      t.string :fullname
      t.text :address
      t.integer :contactno
      t.date :dob
      t.string :gender
      t.string :vendortype
      t.string :contracterno
      t.integer :contractperiod
      t.date :contractstartdate
      t.date :contractenddate
      t.string :panno
      t.float :servicetax
      t.float :tds
      t.float :salary
      t.string :contractername

      t.timestamps null: false
    end
  end
end
