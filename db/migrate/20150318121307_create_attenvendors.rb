class CreateAttenvendors < ActiveRecord::Migration
  def change
    create_table :attenvendors do |t|
      t.string :firstname
      t.string :lastname
      t.date :date
      t.time :timein
      t.time :timeout
      t.string :vendortype
      t.string :phasename

      t.timestamps null: false
    end
  end
end
