class CreateMaintenanceCharges < ActiveRecord::Migration
  def change
    create_table :maintenance_charges do |t|
      t.float :mnt_baseamount
      t.float :mnt_sinkingfnd
      t.float :mnt_otherfund
      t.text :mnt_odesc

      t.timestamps null: false
    end
  end
end
