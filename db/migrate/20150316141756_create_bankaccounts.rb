class CreateBankaccounts < ActiveRecord::Migration
  def change
    create_table :bankaccounts do |t|
      t.string :acc_no
      t.string :acc_name
      t.string :bank_name
      t.string :bank_branch
      t.text :bank_add
      t.float :bank_balance

      t.timestamps null: false
    end
  end
end
