class CreateComplaints < ActiveRecord::Migration
  def change
    create_table :complaints do |t|
      t.string :subject
      t.text :message

      t.timestamps null: false
    end
  end
end
