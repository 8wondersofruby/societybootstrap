class CreateCommanBills < ActiveRecord::Migration
  def change
    create_table :comman_bills do |t|
      t.string :bill_type
      t.date :date
      t.date :due_date
      t.float :bill_ammount
      t.float :due_ammount
      t.string :view

      t.timestamps null: false
    end
  end
end
