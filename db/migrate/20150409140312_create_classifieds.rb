class CreateClassifieds < ActiveRecord::Migration
  def change
    create_table :classifieds do |t|
      t.string :adtitle
      t.text :addesc
      t.string :sale
      t.string :category
      t.float :prize
      t.string :contactno
      t.string :validuntil
      t.string :visibility

      t.timestamps
    end
  end
end
