class CreateGalleryattachments < ActiveRecord::Migration
  def change
    create_table :galleryattachments do |t|
      t.integer :gallery_id
      t.string :avatar

      t.timestamps
    end
  end
end
