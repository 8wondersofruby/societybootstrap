class AddUserIdToFlatowner < ActiveRecord::Migration
  def change
    add_column :flatowners, :user_id, :integer
  end
end
