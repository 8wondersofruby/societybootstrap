class CreateParktenants < ActiveRecord::Migration
  def change
    create_table :parktenants do |t|
      t.integer :nooftwowheeler
      t.integer :nooffourwheeler
      t.integer :ownerid

      t.timestamps null: false
    end
  end
end
