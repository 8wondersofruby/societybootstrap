class CreateNoticeboards < ActiveRecord::Migration
  def change
    create_table :noticeboards do |t|
      t.string :title
      t.text :description
      t.date :validuntil
      t.string :attachment

      t.timestamps null: false
    end
  end
end
