class CreateAttenvisitors < ActiveRecord::Migration
  def change
    create_table :attenvisitors do |t|
      t.string :firstname
      t.string :lastname
      t.date :date
      t.time :timein
      t.time :timeout
      t.string :phasename
      t.integer :flatno
      t.string :mobileno
      t.text :address

      t.timestamps null: false
    end
  end
end
