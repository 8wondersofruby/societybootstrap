class CreateRegowners < ActiveRecord::Migration
  def change
    create_table :regowners do |t|
      t.string :firstname
      t.string :lastname
      t.string :email
      t.string :landlineno
      t.string :mobileno
      t.date :dob
      t.string :gender
      t.integer :age
      t.integer :nooffamily
      t.string :profession
      t.string :pancardno
      t.text :address

      t.timestamps null: false
    end
  end
end
