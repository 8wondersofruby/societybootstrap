# == Schema Information
#
# Table name: noticeboards
#
#  id          :integer          not null, primary key
#  title       :string(255)
#  description :text
#  validuntil  :date
#  attachment  :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :noticeboard do
    title "MyString"
description "MyText"
validuntil "2015-03-18"
  end

end
