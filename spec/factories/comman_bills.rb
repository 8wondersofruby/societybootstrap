# == Schema Information
#
# Table name: comman_bills
#
#  id           :integer          not null, primary key
#  bill_type    :string(255)
#  date         :date
#  due_date     :date
#  bill_ammount :float
#  due_ammount  :float
#  view         :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryGirl.define do
  factory :comman_bill do
    bill_type "MyString"
date "2015-04-02"
due_date "2015-04-02"
bill_ammount 1.5
due_ammount 1.5
view "MyString"
  end

end
