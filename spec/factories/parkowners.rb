# == Schema Information
#
# Table name: parkowners
#
#  id              :integer          not null, primary key
#  parkingno       :integer
#  nooftwowheeler  :integer
#  nooffourwheeler :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :parkowner do
    parkingno 1
nooftwowheeler 1
nooffourwheeler 1
  end

end
