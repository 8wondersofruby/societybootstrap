# == Schema Information
#
# Table name: meetings
#
#  id              :integer          not null, primary key
#  phase           :string(255)
#  pointsdiscussed :text
#  dateandtime     :datetime
#  venue           :string(255)
#  conclusion      :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :meeting do
    phase "MyString"
pointsdiscussed "MyText"
dateandtime "2015-03-17 13:28:24"
venue "MyString"
conclusion "MyText"
  end

end
