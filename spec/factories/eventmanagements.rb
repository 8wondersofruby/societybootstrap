# == Schema Information
#
# Table name: eventmanagements
#
#  id                                                     :integer          not null, primary key
#  EventName                                              :string(255)
#  EventCoordinator                                       :string(255)
#  Email                                                  :string(255)
#  EventDescription                                       :text
#  EventType                                              :string(255)
#  Does_Your_Program_involve_Any_Type_Of_Outside_Activity :string(255)
#  Requirements                                           :string(255)
#  EventStartDate                                         :datetime
#  EventEndDate                                           :datetime
#  Location                                               :text
#  city                                                   :string(255)
#  created_at                                             :datetime         not null
#  updated_at                                             :datetime         not null
#  image_file_name                                        :string(255)
#  image_content_type                                     :string(255)
#  image_file_size                                        :integer
#  image_updated_at                                       :datetime
#

FactoryGirl.define do
  factory :eventmanagement do
    EventName "MyString"
EventCoordinator "MyString"
Email "MyString"
EventDescription "MyText"
EventType "MyString"
Does_Your_Program_involve_Any_Type_Of_Outside_Activity "MyString"
Requirements "MyString"
EventStartDate "2015-03-19 20:01:00"
EventEndDate "2015-03-19 20:01:00"
Location "MyText"
city "MyString"
  end

end
