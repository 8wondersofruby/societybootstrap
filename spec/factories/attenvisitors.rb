# == Schema Information
#
# Table name: attenvisitors
#
#  id         :integer          not null, primary key
#  firstname  :string(255)
#  lastname   :string(255)
#  date       :date
#  timein     :time
#  timeout    :time
#  phasename  :string(255)
#  flatno     :integer
#  mobileno   :string(255)
#  address    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :attenvisitor do
    firstname "MyString"
lastname "MyString"
date "2015-03-18"
timein "2015-03-18 16:28:36"
timeout "2015-03-18 16:28:36"
phasename "MyString"
flatno 1
mobileno "MyString"
address "MyText"
  end

end
