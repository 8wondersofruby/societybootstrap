# == Schema Information
#
# Table name: nr_incomes
#
#  id               :integer          not null, primary key
#  nri_type         :string(255)
#  bank_account     :integer
#  member_type      :string(255)
#  name             :string(255)
#  phase_no         :integer
#  flat_no          :integer
#  amount           :float
#  date             :date
#  chq_desc         :text
#  chq_no           :string(255)
#  chqbank_name     :string(255)
#  chqbranch_name   :string(255)
#  chq_date         :date
#  trs_desc         :text
#  trs_bankname     :string(255)
#  trs_no           :string(255)
#  trs_date         :date
#  draft_des        :text
#  draft_no         :string(255)
#  draft_bankname   :string(255)
#  draft_branchname :string(255)
#  draft_date       :date
#  cash_desc        :text
#  cash_date        :date
#  pay_mode         :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

FactoryGirl.define do
  factory :nr_income do
    nri_type "MyString"
bank_account "MyString"
member_type "MyString"
name "MyString"
phase_no 1
flat_no 1
amount 1.5
date "2015-03-23"
chq_desc "MyText"
chq_no "MyString"
chqbank_name "MyString"
chqbranch_name "MyString"
chq_date "2015-03-23"
trs_desc "MyText"
trs_bankname "MyString"
trs_no "MyString"
trs_date "2015-03-23"
draft_des "MyText"
draft_no "MyString"
draft_bankname "MyString"
draft_branchname "MyString"
draft_date "2015-03-23"
cash_desc "MyText"
cash_date "2015-03-23"
pay_mode "MyString"
  end

end
