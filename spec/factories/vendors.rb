# == Schema Information
#
# Table name: vendors
#
#  id                :integer          not null, primary key
#  fullname          :string(255)
#  address           :text
#  contactno         :integer
#  dob               :date
#  gender            :string(255)
#  vendortype        :string(255)
#  contracterno      :string(255)
#  contractperiod    :integer
#  contractstartdate :date
#  contractenddate   :date
#  panno             :string(255)
#  servicetax        :float
#  tds               :float
#  salary            :float
#  contractername    :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

FactoryGirl.define do
  factory :vendor do
    fullname "MyString"
address "MyText"
contactno 1
dob "2015-03-18"
gender "MyString"
vendortype "MyString"
contracterno "MyString"
contractperiod 1
contractstartdate "2015-03-18"
contractenddate "2015-03-18"
panno "MyString"
servicetax 1.5
tds 1.5
salary 1.5
contractername "MyString"
  end

end
