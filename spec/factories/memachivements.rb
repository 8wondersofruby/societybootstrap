# == Schema Information
#
# Table name: memachivements
#
#  id                 :integer          not null, primary key
#  FullName           :string(255)
#  Achivement         :string(255)
#  Description        :text
#  FlatNo             :integer
#  Phase              :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#

FactoryGirl.define do
  factory :memachivement do
    FullName "MyString"
Achivement "MyString"
Description "MyText"
FlatNo 1
Phase "MyString"
  end

end
