# == Schema Information
#
# Table name: flatowners
#
#  id         :integer          not null, primary key
#  phasename  :string(255)
#  subphase   :string(255)
#  flatno     :integer
#  flattype   :string(255)
#  flatarea   :float
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#

FactoryGirl.define do
  factory :flatowner do
    phasename "MyString"
subphase "MyString"
flatno 1
flattype "MyString"
flatarea 1.5
  end

end
