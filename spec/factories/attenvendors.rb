# == Schema Information
#
# Table name: attenvendors
#
#  id         :integer          not null, primary key
#  firstname  :string(255)
#  lastname   :string(255)
#  date       :date
#  timein     :time
#  timeout    :time
#  vendortype :string(255)
#  phasename  :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :attenvendor do
    firstname "MyString"
lastname "MyString"
date "2015-03-18"
timein "2015-03-18 17:43:07"
timeout "2015-03-18 17:43:07"
vendortype "MyString"
phasename "MyString"
  end

end
