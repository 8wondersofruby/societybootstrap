# == Schema Information
#
# Table name: maintainnance_invoices
#
#  id          :integer          not null, primary key
#  iv_year     :string(255)
#  iv_freq     :string(255)
#  iv_period   :string(255)
#  iv_fromdate :date
#  iv_todate   :date
#  invoicedate :date
#  iv_duedate  :date
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :maintainnance_invoice do
    iv_year "MyString"
iv_freq "MyString"
iv_period "MyString"
iv_fromdate "2015-03-19"
iv_todate "2015-03-19"
invoicedate "2015-03-19"
iv_duedate "2015-03-19"
  end

end
