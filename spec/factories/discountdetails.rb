# == Schema Information
#
# Table name: discountdetails
#
#  id         :integer          not null, primary key
#  discdesc   :text
#  disamount  :float
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :discountdetail do
    discdesc "MyText"
disamount 1.5
  end

end
