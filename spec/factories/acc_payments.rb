# == Schema Information
#
# Table name: acc_payments
#
#  id            :integer          not null, primary key
#  chq_desc      :text
#  chq_no        :string(255)
#  bnk_name      :string(255)
#  bnk_branch    :string(255)
#  chq_date      :date
#  cash_desc     :text
#  paym_date     :date
#  drft_desc     :text
#  drft_no       :string(255)
#  dbnk_name     :string(255)
#  dbnk_branch   :string(255)
#  drft_date     :date
#  ot_desc       :text
#  otbnk_name    :string(255)
#  trn_no        :string(255)
#  ot_date       :date
#  paymode       :string(255)
#  exbill_date   :date
#  exbill_desc   :text
#  exbill_amount :float
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryGirl.define do
  factory :acc_payment do
    chq_desc "MyText"
chq_no "MyString"
bnk_name "MyString"
bnk_branch "MyString"
chq_date "2015-03-24"
cash_desc "MyText"
paym_date "2015-03-24"
drft_desc "MyText"
drft_no "MyString"
dbnk_name "MyString"
dbnk_branch "MyString"
drft_date "2015-03-24"
ot_desc "MyText"
otbnk_name "MyString"
trn_no "MyString"
ot_date "2015-03-24"
paymode "MyString"
exbill_date "2015-03-24"
exbill_desc "MyText"
exbill_amount 1.5
  end

end
