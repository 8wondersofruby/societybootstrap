# == Schema Information
#
# Table name: comitteemembers
#
#  id             :integer          not null, primary key
#  fullname       :string(255)
#  gender         :string(255)
#  role           :string(255)
#  email          :string(255)
#  phaseno        :string(255)
#  responsiblefor :text
#  startdate      :date
#  duration       :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

FactoryGirl.define do
  factory :comitteemember do
    fullname "MyString"
gender "MyString"
role "MyString"
email "MyString"
phaseno 1
responsiblefor "MyText"
startdate "2015-03-16"
duration 1
  end

end
