# == Schema Information
#
# Table name: documents
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  attachment :string(255)
#  date       :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :document do
    title "MyString"
attachment "MyString"
date "2015-04-02 17:27:15"
  end

end
