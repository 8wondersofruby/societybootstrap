# == Schema Information
#
# Table name: maintenance_charges
#
#  id             :integer          not null, primary key
#  mnt_baseamount :float
#  mnt_sinkingfnd :float
#  mnt_otherfund  :float
#  mnt_odesc      :text
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

FactoryGirl.define do
  factory :maintenance_charge do
    mnt_baseamount 1.5
mnt_sinkingfnd 1.5
mnt_otherfund 1.5
mnt_odesc "MyText"
  end

end
