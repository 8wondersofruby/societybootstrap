# == Schema Information
#
# Table name: bankaccounts
#
#  id           :integer          not null, primary key
#  acc_no       :string(255)
#  acc_name     :string(255)
#  bank_name    :string(255)
#  bank_branch  :string(255)
#  bank_add     :text
#  bank_balance :float
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryGirl.define do
  factory :bankaccount do
    acc_no 1
acc_name "MyString"
bank_name "MyString"
bank_branch "MyString"
bank_add "MyText"
bank_balance 1.5
  end

end
