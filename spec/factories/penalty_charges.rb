# == Schema Information
#
# Table name: penalty_charges
#
#  id            :integer          not null, primary key
#  pn_desc       :text
#  pn_ovrdueamt  :float
#  pn_ovrduedays :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryGirl.define do
  factory :penalty_charge do
    pn_desc "MyText"
pn_ovrdueamt 1.5
pn_ovrduedays 1
  end

end
