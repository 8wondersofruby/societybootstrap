# == Schema Information
#
# Table name: acc_payments
#
#  id            :integer          not null, primary key
#  chq_desc      :text
#  chq_no        :string(255)
#  bnk_name      :string(255)
#  bnk_branch    :string(255)
#  chq_date      :date
#  cash_desc     :text
#  paym_date     :date
#  drft_desc     :text
#  drft_no       :string(255)
#  dbnk_name     :string(255)
#  dbnk_branch   :string(255)
#  drft_date     :date
#  ot_desc       :text
#  otbnk_name    :string(255)
#  trn_no        :string(255)
#  ot_date       :date
#  paymode       :string(255)
#  exbill_date   :date
#  exbill_desc   :text
#  exbill_amount :float
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe AccPayment, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
